#! /usr/bin/env python2

def options(opt):
    pass

def configure(cfg):
    pass

def build(bld):
    bld.shlib(
        source = '''
            src/rules.cc
            src/entry.cc
            src/map.cc
            src/unit.cc
            src/cell.cc
            src/ability.cc
            src/game.cc
            src/api.cc
            src/action-move.cc
            src/action-attack.cc
            src/action-ack.cc
            src/dumper.cc
            src/interface.cc
        ''',
        target = 'prologin2012',
        use = ['stechec2'],
        defines = ['MODULE_COLOR=ANSI_COL_PURPLE',
            'MODULE_NAME="prologin2012"'],
        lib = ['dl'],
    )

    for test in ['unit', 'cell', 'ability', 'map', 'game', 'api']:
        bld.program(
            features = 'gtest',
            source = 'src/tests/test-%s.cc' % test,
            target = 'prologin2012-test-%s' % test,
            use = ['prologin2012', 'stechec2-utils'],
            includes = ['.'],
            defines = ['MODULE_COLOR=ANSI_COL_PURPLE',
                'MODULE_NAME="prologin2012"'],
        )

    bld.install_files('${PREFIX}/share/stechec2/prologin2012', [
        'prologin2012.yml',
    ])

    bld.install_files('${PREFIX}/share/stechec2/prologin2012/www/css', [
        'www/css/main.css',
    ])
    bld.install_files('${PREFIX}/share/stechec2/prologin2012/www/img', [
        'www/img/back.png',
        'www/img/head.png',
        'www/img/medieval-abstract-ribbons-vector-547986.jpg',
        'www/img/medieval-ribbons-8b1a18.jpg',
    ])
    bld.install_files('${PREFIX}/share/stechec2/prologin2012/www/font', [
        'www/font/exocet-heavy.ttf',
    ])

